package com.company.app;

import com.company.app.components.MainMenu;
import com.company.database.DataBase;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {
    public MainFrame(){
        setTitle("CBooK");
        setSize(500, 500);
        setJMenuBar(new MainMenu());

        DataBase.getData("contact", "id, name, surname");
        table = new JTable(MainFrame.tableModel);
        JScrollPane scroll = new JScrollPane(table);
        panel.add(scroll);
        table.setComponentPopupMenu(getPopupMenu());
        add(panel);

        setLocation(2000, 300);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private JPopupMenu getPopupMenu(){
        JPopupMenu popup = new JPopupMenu();
        JMenuItem open = new JMenuItem("Посмотреть");
        JMenuItem edit = new JMenuItem("Редактировать");
        JMenuItem delete = new JMenuItem("Удалить");

        popup.add(open);
        popup.add(edit);
        popup.add(delete);

        delete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row = table.getSelectedRow();
                while (row != -1){
                    int modelRow = table.convertRowIndexToModel(row);
                    String id = table.getModel().getValueAt(row, 0).toString();
                    DataBase.delete("contact", id);
                    tableModel.removeRow(modelRow);
                    row = table.getSelectedRow();
                }
            }
        });

        return popup;
    }

    JPanel panel = new JPanel();
    public static JTable table;
    public static String[] titles = {"ID", "Имя", "Фамилия", "Телефон", "Email"};
    public static DefaultTableModel tableModel = new DefaultTableModel(titles, 0);
}
