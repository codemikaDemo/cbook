package com.company.app.components;

import com.company.app.MainFrame;
import com.company.app.components.MainMenu;
import com.company.database.DataBase;
import com.company.domain.Contact;
import com.company.domain.Email;
import com.company.domain.Phone;
import com.company.error.FormError;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class CreateContact extends JFrame {
    public CreateContact(){
        setTitle("Создать контакт");
        setSize(400, 300);
        setJMenuBar(new MainMenu());

        add(createPanel());

        setLocation(2050, 350);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private JPanel createPanel(){
        add(error);
        name = addTextField("Введите имя", panel);
        surname = addTextField("Введите фамилию", panel);
        phone = addTextField("Введите телефон", panel);
        email = addTextField("Введите email", panel);

        addButtonSave("Сохранить", panel, this);

        return panel;
    }

    private JButton addButtonSave(String title, JComponent component, JFrame frame) {
        JButton button = new JButton("Сохранить");
        component.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FormError.list.clear();
                Phone userPhone = new Phone(phone.getText());
                Email userEmail = new Email(email.getText());

                if (FormError.list.size() > 0)
                    error.setText(FormError.list.get(0));
                else {
                    Contact user = new Contact(
                            name.getText(),
                            surname.getText()
                    );
                    user.addPhone(userPhone);
                    user.addEmail(userEmail);
                    error.setText("");
//                    Object[] data = {name.getText(), surname.getText()};
//                    MainFrame.tableModel.addRow(data);
                    MainFrame.tableModel.setRowCount(0);
                    DataBase.getData("contact", "id, name, surname");
                    frame.setVisible(false);
                }
            }
        });

        return button;
    }

    private JTextField addTextField(String title, JComponent component){
        JTextField field = new JTextField(title);
        field.setText(title);
        component.add(field);
        field.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (field.getText().equals(title))
                    field.setText("");
            }
            @Override
            public void focusLost(FocusEvent e) {
                if (field.getText().equals(""))
                    field.setText(title);
            }
        });
        return field;
    }

    JPanel panel = new JPanel();
    private static JTextField name;
    private static JTextField surname;
    private static JTextField phone;
    private static JTextField email;
    private static JLabel error = new JLabel("");
}












