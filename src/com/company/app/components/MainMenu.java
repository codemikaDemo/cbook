package com.company.app.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenu extends JMenuBar {
    public MainMenu(){
        JMenu file = addMenu("Файл", this);
        JMenu newFile = addMenu("Новый", file);
        JMenuItem newContact = addMenuItem("Контакт", newFile);
        JMenuItem close = addMenuItem("Закрыть", file);

        JMenu edit = addMenu("Правка", this);
        JMenu operation = addMenu("Операции", edit);
        JMenuItem copy = addMenuItem("Копировать", operation);
        JMenuItem cut = addMenuItem("Вырезать", operation);
        JMenuItem paste = addMenuItem("Вставить", operation);

        JMenu help = addMenu("Помощь", this);

        newContact.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CreateContact();
            }
        });
    }

    public JMenu addMenu(String name, JComponent parent){
        Font font = new Font("Verdana", Font.PLAIN, 14);
        JMenu menu = new JMenu(name);
        menu.setFont(font);
        parent.add(menu);
        return menu;
    }

    public JMenuItem addMenuItem(String name, JComponent parent){
        Font font = new Font("Verdana", Font.PLAIN, 12);
        JMenuItem menuItem = new JMenuItem(name);
        menuItem.setFont(font);
        parent.add(menuItem);
        return menuItem;
    }
}
