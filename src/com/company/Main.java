package com.company;

import com.company.app.MainFrame;
import com.company.database.DataBase;

public class Main {
    public static void main(String[] args) {
        new MainFrame();
        DataBase.readSQL("contact", "name, surname");
    }
}