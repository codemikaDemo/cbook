package com.company.domain;

import com.company.error.FormError;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
    private String address;
    public Email(String address){
        this.address = set(address);
    }
    public String set(String email){
        if (isEmailValid(email)) {
            return email;
        }
        FormError.list.add("Email не валиден");
        return "error";
    }
    public String get(){
        return this.address;
    }
    public static boolean isEmailValid(String email){
        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
